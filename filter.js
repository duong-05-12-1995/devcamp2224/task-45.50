"use strict"
    $(document).ready(function(){
       
    //Vùng 1: định nghĩa global variable (biến toàn cục)
    const gREQUEST_STATUS_OK = 200;
    const gREQUEST_CREATE_OK = 201; // status 201 tao thanh cong
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

    var gCallObj = [];
    const ARRAY_COLUM = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    const ORDERID_COL = 0;
    const KICHCO_COL = 1;
    const LOAIPIZZA_COL = 2;
    const NUOCUONG_COL = 3;
    const THANHTIEN_COL = 4;
    const HOTEN_COL = 5;
    const SODIENTHOAI_COL = 6;
    const TRANGTHAI_COL = 7;
    const ACTION_COL = 8;

     var gId ;
     var gOrderId ;
    var gPizzaSize = ["S","M" ,"L"];

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    getDrinkList();
    getPizzaSizeList(gPizzaSize);


    $("#Table-User").on("click", ".btn-detail", function(){
        onBtnChiTietClick(this);
    });
    $("#btn-loc").on("click",  function(){
        onBtnlocClick();
    });
    
    $("#btn-Confirm").on("click",  function(){
        onBtnConfirmlick();
    });
    $("#btn-Cancel").on("click",  function(){
        onBtnCancellick();
    });
    

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        callApiServerObj();

        $("#Table-User").DataTable({
        'columns': [
            {"data" : ARRAY_COLUM[ORDERID_COL]},
            {"data" : ARRAY_COLUM[KICHCO_COL]},
            {"data" : ARRAY_COLUM[LOAIPIZZA_COL]},
            {"data" : ARRAY_COLUM[NUOCUONG_COL]},
            {"data" : ARRAY_COLUM[THANHTIEN_COL]},
            {"data" : ARRAY_COLUM[HOTEN_COL]},
            {"data" : ARRAY_COLUM[SODIENTHOAI_COL]},
            {"data" : ARRAY_COLUM[TRANGTHAI_COL]},
            {"data" : ARRAY_COLUM[ACTION_COL]},
        ],
        // ghi đè nội dung của cột action , chuyển thành button chi tiết
        "columnDefs": [
            {
            "targets": ACTION_COL,
            "defaultContent": `<button class="btn btn-success btn-detail">Chi Tiết</button>`
        }
        ] 
    });

    
    };
    // hàm xử lý sự kiện nút confirm 
    function onBtnConfirmlick(){
        var vTrangThai = {
            trangThai: "Confirmed"
        };
        var vInputSend = JSON.stringify(vTrangThai);
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + gId,
            type: "PUT",
            data: vInputSend,
            contentType:"application/json",
            success: function(dataRespon){
                alert("ok")
                $("#Modal-Order").modal("hide");
                console.log(dataRespon);
                loadDataToTable(dataRespon);
            },
            error: function(error){
                alert("Không Lấy Được Dũ Liệu Từ Server")
            }
        })

    };

    // hàm xử lý sự kiện nút cancel
    function onBtnCancellick(){
        var vTrangThai = {
            trangThai: "cancel"
        };
        var vInputSend = JSON.stringify(vTrangThai);
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + gId,
            type: "PUT",
            data: vInputSend,
            contentType:"application/json",
            success: function(dataRespon){
                alert("ok")
                $("#Modal-Order").modal("hide");
                console.log(dataRespon);
                loadDataToTable(dataRespon);
            },
            error: function(error){
                alert("Không Lấy Được Dũ Liệu Từ Server")
            }
        })
    }
    // hàm xử lý sự kiện khi click nút lọc
    function onBtnlocClick(){
        console.log("ok")
        "use strict";
        var vTrangThaiSelect = $("#select-trangthai").val();
        var vLoaiPizzaSelect = $("#select-loai-Pizza").val();
        var vDataFilter = gCallObj.filter(function(paramOrder, index){
            return  (vTrangThaiSelect === "" || vTrangThaiSelect.toUpperCase() === paramOrder.trangThai.toUpperCase()) &&
                    (vLoaiPizzaSelect === "" || vLoaiPizzaSelect.toUpperCase() === paramOrder.loaiPizza.toUpperCase())

        });
        loadDataToTable(vDataFilter);
    }
    
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function callApiServerObj(){
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType : "Json",
            success: function(dataRespon){
                console.log("ok")
                gCallObj = dataRespon;
                console.log(dataRespon);
                loadDataToTable(dataRespon);
            },
            error: function(error){
                alert("Không Lấy Được Dũ Liệu Từ Server")
            }
        })
        
    }


    // hàm load dữ liệu lấy được vào bảng
    function loadDataToTable(paramObj){
        "use strict";
      var vTable = $('#Table-User').DataTable();
      vTable.clear();
      vTable.rows.add(paramObj);
      vTable.draw();
    };

    // hàm xử lý khi ấn nút chi tiết
    function onBtnChiTietClick(paramElement){
        console.log("ok")
        "use strict"
        var vRowSelect = $(paramElement).closest("tr");
        var vTable2 = $('#Table-User').DataTable();
        var vRowData = vTable2.row(vRowSelect).data();
        gId = vRowData.id;
        gOrderId = vRowData.orderId;
        console.log("id là : "  + gId);
        console.log("OrderId là : "  + gOrderId);
        getAjaxDetailOrder(gOrderId);
        $("#Modal-Order").modal("show");
    }
    // hàm gọi Api Select Drink
    function getDrinkList(){
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType : "Json",
            success: function(dataRespon){
                console.log(dataRespon);
                handleDrinkList(dataRespon);
            },
            error: function(error){
                alert(error.responseText)
            }
        })
    };
    // hàm tạo option cho select đồ uống
    function handleDrinkList(paramDrink){
        "use strict";
        $.each(paramDrink,function(i, item){
            $("#select-douong").append($("<option>", {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }));

        })
    }
    // hàm tạo option cho select pizza
    function getPizzaSizeList(paramPizza){
        "use strict";
        $.each(paramPizza,function(i, item){
            $("#pizza-size-inp").append($("<option>", {
                text: item,
                value: item.toLowerCase()
            }));

        })
    };

    // hàm gọi order theo orderid
    function getAjaxDetailOrder(paramOrderId){
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramOrderId,
            type: "GET",
            dataType : "Json",
            success: function(dataRespon){
                handleOrderDetail(dataRespon);
                console.log(dataRespon);
            },
            error: function(error){
                alert(error.responseText)
            }
        })

    };
    // hàm đổ dâta vào modal
    function handleOrderDetail(paramObj){
        "use strict";
        $("#inp-Id").val(paramObj.id);
        $("#inp-OrderId").val(paramObj.orderId);
        $("#pizza-size-inp").val(paramObj.kichCo.toLowerCase());
        $("#inp-duongkinh").val(paramObj.duongKinh);
        $("#inp-suon").val(paramObj.suon);
        $("#inp-Salad").val(paramObj.salad);
        $("#inp-Pizza-Type").val(paramObj.loaiPizza);
        $("#inp-IdVoucher").val(paramObj.idVourcher);
        $("#inp-Thanhtien").val(paramObj.thanhTien);
        $("#inp-GiamGia").val(paramObj.giamGia);
        $("#select-douong").val(paramObj.idLoaiNuocUong);
        $("#inp-SLN").val(paramObj.soLuongNuoc);
        $("#inp-name").val(paramObj.hoTen);
        $("#inp-Email").val(paramObj.email);
        $("#inp-SDT").val(paramObj.soDienThoai);
        $("#inp-address").val(paramObj.diaChi);
        $("#inp-message").val(paramObj.loiNhan);
        $("#inp-order-day").val(paramObj.ngayTao);
        $("#inp-repairDay").val(paramObj.ngayCapNhat);

    }

    })